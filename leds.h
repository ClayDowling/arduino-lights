#ifndef LEDS_H
#define LEDS_H

#include <FastLED.h>

#define NUM_LEDS 1200

#define BANK_1_LEN 400
#define BANK_2_LEN 400


extern CRGB led[NUM_LEDS];

#endif