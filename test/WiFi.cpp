#include "WiFi.h"

void WiFi::begin(const char* ssid) {
    ssidCallValue = ssid;
}

WiFi::WL_STATUS WiFi::status(void) {
    return statusReturnValue;
}

const char* WiFi::beginCalledWith() {
    return ssidCallValue;
}

void WiFi::statusWillReturn(WiFi::WL_STATUS status) {
    statusReturnValue = status;
}