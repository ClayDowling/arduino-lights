#ifndef MOCK_WIFI_H
#define MOCK_WIFI_H

class WiFi {

public:
    void begin(const char* ssid);
    enum WL_STATUS {
        WL_CONNECTED,
        WL_NO_SHIELD,
        WL_IDLE_STATUS,
        WL_NO_SSID_AVAIL,
        WL_SCAN_COMPLETED,
        WL_CONNECT_FAILED,
        WL_CONNECTION_LOST,
        WL_DISCONNECTED
    };
    WL_STATUS status(void);

    const char* beginCalledWith();
    void statusWillReturn(WL_STATUS);

private:
    const char *ssidCallValue;
    WL_STATUS statusReturnValue;

};

#endif