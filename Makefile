.PHONY: arduino-lights clean

arduino-lights: arduino-lights.ino
	arduino-cli compile

flash: arduino-lights
	arduino-cli upload -p COM3