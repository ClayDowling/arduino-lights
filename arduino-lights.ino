
#include <FastLED.h>
#include "leds.h"

#define DATA_PIN_1 27
#define DATA_PIN_2 26

void setup() {

    FastLED.addLeds<WS2812B, DATA_PIN_1, GRB>(&led[0], NUM_LEDS);
    FastLED.addLeds<WS2812B, DATA_PIN_2, GRB>(&led[BANK_1_LEN], NUM_LEDS);
}

void setOneColor(CRGB color, CRGB* array) {
    for(int i=0; i < NUM_LEDS; ++i) {
        array[i] = color;
    }
}

void fadeFromBlack(HSVHue hue, CRGB* array) {
    for(int i = 0; i < 255; ++i) {
        setOneColor(CHSV(hue, 255, i), array);
        FastLED.show();
        delay(50);
    }   
}

void fadeToBlack(HSVHue hue, CRGB* array) {
    for(int i = 255; i > 0; --i) {
        setOneColor(CHSV(hue, 255, i), array);
        FastLED.show();
        delay(50);
    }   
}

void fadeToWhite(HSVHue hue, CRGB* array) {
    for(int i = 255; i > 0; --i) {
        setOneColor(CHSV(hue, i, 255), array);
        FastLED.show();
        delay(50);
    }   
}

void fadeFromWhite(HSVHue hue, CRGB* array) {
    for(int i = 0; i < 255; ++i) {
        setOneColor(CHSV(hue, i, 255), array);
        FastLED.show();
        delay(50);
    }   
}


void loop() {

    fadeFromBlack(HUE_GREEN, led_1);
    fadeToWhite(HUE_RED, led_2);
    fadeToBlack(HUE_GREEN, led_1);
    fadeFromWhite(HUE_BLUE, led_2);

    fadeFromBlack(HUE_RED, led_1);
    fadeToWhite(HUE_BLUE, led_2);
    fadeToBlack(HUE_RED, led_1);
    fadeFromWhite(HUE_RED, led_2);

}
